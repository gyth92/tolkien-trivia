import { Injectable } from '@angular/core';
import * as mockQuestions from './mockData/mockQuestions.json';

@Injectable({
  providedIn: 'root'
})
export class QuizDataService {
  
  constructor() {
   }

  getQuiz() {
    //at some point this will be making a call to a DB, will need to be a-sync
    let questions = [];
    questions = this.shuffle();

    return questions;
  }

  shuffle() {
    let shuffledQuestions = [];
      
    shuffledQuestions = mockQuestions.default.sort(this.randomize);

    //now shuffle choices
    for(let i = 0; i < shuffledQuestions.length; i++) {
      shuffledQuestions[i].choices.sort(this.randomize);
    }

    return shuffledQuestions;
  }

  randomize(a, b) {
    return Math.random() - 0.5;
  }


  
}
