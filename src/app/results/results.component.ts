import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import * as url1 from '../mockData/f-.gif';
import * as url2 from '../mockData/f.gif';
import * as url3 from '../mockData/d-.gif';
import * as url4 from '../mockData/D.gif';
import * as url5 from '../mockData/c.gif';
import * as url6 from '../mockData/b-.gif';
import * as url7 from '../mockData/b.gif';
import * as url8 from '../mockData/a-.gif';
import * as url9 from '../mockData/a.gif';
import * as url10 from '../mockData/a+.gif';


@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit {



  userScore = "";
  percentGrade;
  letterGrade = "";
  gifResponse;

  constructor(private route: ActivatedRoute) { 

  }

  ngOnInit() {
    this.userScore = this.route.snapshot.paramMap.get("score")
    this.percentGrade = parseInt(this.userScore);
    console.log('userScore: ', typeof this.userScore)

    this.letterGrade = this.getLetterGrade(this.percentGrade);

  }

  getLetterGrade(score){
    let letter = "";

    if(score <= 39) {
      letter = "F-";
      this.gifResponse = url1;
    } else if(score <= 59) {
      letter = "F";
      this.gifResponse = url2;
    } else if((score >= 60)&&(score <= 64)){
      letter = "D-"
      this.gifResponse = url3;

    } else if((score >= 65) && (score <= 69)) {
      letter = "D+";
      this.gifResponse = url4;
    }
    else if(score <= 79) {
      letter = "C";
      this.gifResponse = url5;
    } else if ((score >= 80)&&(score <= 85)) {
      letter = "B-"
      this.gifResponse = url6;
    } else if((score >= 85)&&(score <= 89)) {
      letter = "B+";
      this.gifResponse = url7;
    } else if((score >= 90)&&(score <=94)) {
      letter = "A-";
      this.gifResponse = url8;
    } else if ((score >= 95)&&(score <= 99)) {
      letter = "A";
      this.gifResponse = url9;
    } else if(score == 100) {
      letter = "A+";
      this.gifResponse = url10;
    }
    return letter;
  }

}
