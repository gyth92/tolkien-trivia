import { Component, OnInit } from '@angular/core';
import * as url from '../mockData/tolkien_trivia_intro.gif';

@Component({
  selector: 'app-welcome-page',
  templateUrl: './welcome-page.component.html',
  styleUrls: ['./welcome-page.component.css']
})
export class WelcomePageComponent implements OnInit {

  source = url.default;

  constructor() { }

  ngOnInit() {
  }

}
