import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WelcomePageComponent } from './welcome-page/welcome-page.component' ;
import { QuizComponent } from './quiz/quiz.component';
import { ResultsComponent } from './results/results.component';


const routes: Routes = [
  { path: 'welcome', component: WelcomePageComponent },
  { path: '', redirectTo: '/welcome', pathMatch: 'full' },
  { path: 'quiz', component: QuizComponent},
  { path: 'results/:score', component: ResultsComponent },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
