import { Component, OnInit } from '@angular/core';
import { QuizDataService } from '../quiz-data.service';
import { trigger, state, transition, animate, style } from '@angular/animations'

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.css'],
  // animations: [
  //   trigger('slideIn', [
  //     transition(':enter', [
  //       style({transform: 'translateX(100%)'}),
  //       animate('500ms ease-in', style({transform: 'translateX(0%)'}))
  //     ]),
  //   ]),
  //       transition(':leave', [
  //         animate('500ms ease-in', style({transform: 'translateX(-100%)'}))
  //       ])
  // ]
})
export class QuizComponent implements OnInit {
  quiz;
  questionText = "";
  answer = "";
  choices = [];

  currentIndex = 0;
  selectedAnswer = "";
  totalQuestions = 10;
  userScore = 0;
  showResultsButton = false;
  nextQuestion = 1;
  visible = false;
  transform = 0;
  slideIndex = 1;
  score = 0;
  choiceSelectedValidator = false;
  showResultCard = false;

  constructor(private quizService: QuizDataService) {}
  

  ngOnInit() {
    this.quiz = this.quizService.getQuiz()
    
    this.presentQuestion(this.quiz, 0);

  }

  presentQuestion(quiz, index) {
    let testJson = {
      "question": "Are we at the end?",
      "choices": ["lol"],
      "answer": "Quendi"
  };
    this.choices = [];
    this.questionText = quiz[index].question;
    this.answer = quiz[index].answer;
    this.choiceSelectedValidator = false;

    for(let i = 0; i < quiz[index].choices.length; i++){
      this.choices.push(quiz[index].choices[i]);
    }

    if(index == 8) {
      quiz.push(testJson);
    }

  }

  choiceSelected(choice) {
    this.selectedAnswer = choice;

    if(choice != "" || choice != null) {
      this.choiceSelectedValidator = true;
    }

  }

  submit() {
    this.currentIndex++;
    this.nextQuestion++;
    if(this.selectedAnswer === this.answer){
      this.userScore++;
    }
    console.log('currentIndex', this.currentIndex);
    if(this.currentIndex == 10) {
      this.showResultsButton = true;
      this.showResultCard = true;
    } 

    this.presentQuestion( this.quiz, this.currentIndex)
    this.calculateTotal();
  }

  calculateTotal() {
    this.score = (this.userScore / this.totalQuestions) * 100;
  }
}
