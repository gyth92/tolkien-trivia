import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuizComponent } from './quiz.component';

fdescribe('QuizComponent', () => {
  let component: QuizComponent;
  let fixture: ComponentFixture<QuizComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuizComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // it('should have the welcome page text on the page', () => {
  //   const fixture = TestBed.createComponent(QuizComponent);
  //   const compiled = fixture.debugElement.nativeElement;
  //   expect(compiled.querySelector('p').textContent).toContain('quiz works!');
  // });

  // it('should have an anchor tag that will lead the user to the quiz component', () => {
  //   const fixture = TestBed.createComponent(QuizComponent);
  //   const compiled = fixture.debugElement.nativeElement;
  //   console.log(compiled)
  //   expect(compiled.querySelector('a').textContent).toContain('Results Page');
  // });
});
